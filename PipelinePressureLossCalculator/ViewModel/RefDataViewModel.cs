﻿using System;
using PipelinePressureLossCalculator.Model.ReferenceData;

namespace PipelinePressureLossCalculator.ViewModel
{
    /// <summary>
    /// Обработка окна Справочные данные
    /// </summary>
    class RefDataViewModel:BaseViewModel
    {
        /// <summary>
        /// Вид справочных данных
        /// </summary>
        private readonly RefDataType _refDataType;
        /// <summary>
        /// Название формы
        /// </summary>
        public string WindowTitle { get; set; }

        private string _imagePath;
        /// <summary>
        /// Изображение справочных данных
        /// </summary>
        public string ImagePath
        {
            get => _imagePath;
            set
            {
                _imagePath = value;
                OnPropertyChanged();
            }
        }


        public RefDataViewModel(RefDataType refDataType)
        {
            this._refDataType = refDataType;
            DefineWindowTitle();            
        }

        public void ShowData()
        {
            IRefData refDataLoader = null;
            switch (_refDataType)
            {
                case RefDataType.KinematicViscosity:
                    refDataLoader = new KinematicViscosityRefData();
                    break;
                case RefDataType.EquivRoughness:
                    refDataLoader = new EquivRoughnessRefData();
                    break;
                case RefDataType.Density:
                    refDataLoader = new DensityRefData();
                    break;
            }

            if (refDataLoader == null)
            {
                throw new NullReferenceException("refDataLoader wasn't defined.");
            }

            ImagePath = refDataLoader.Load().ImagePath;
        }

        /// <summary>
        /// Задаёт отображаемое имя окна
        /// </summary>
        private void DefineWindowTitle() {
            string windowTitle = null;
            switch (_refDataType) {
                case RefDataType.KinematicViscosity:
                    windowTitle = "Кинематическая вязкость";
                    break;
                case RefDataType.Density:
                    windowTitle = "Плотность";
                    break;
                case RefDataType.EquivRoughness:
                    windowTitle = "Экв.шероховатость";
                    break;
            }
            WindowTitle = windowTitle+" - "+MainViewModel.AppTitle;
        }


    }



    /// <summary>
    /// Вид справочных данных
    /// </summary>
    enum RefDataType
    {
        /// <summary>
        /// Кинем. вязкость
        /// </summary>
        KinematicViscosity,
        /// <summary>
        /// Плотность
        /// </summary>
        Density,
        /// <summary>
        /// Экв.шероховатость
        /// </summary>
        EquivRoughness
    }
}


