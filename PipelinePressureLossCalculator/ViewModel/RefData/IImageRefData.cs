﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Media.Imaging;

namespace PipelinePressureLossCalculator.ViewModel.RefData
{
    /// <summary>
    /// Справочные данные в виде изображения
    /// </summary>
    interface IImageRefData
    {
        BitmapImage Show();
    }
}
