﻿using log4net;
using PipelinePressureLossCalculator.Model;
using PipelinePressureLossCalculator.Model.Converter;
using PipelinePressureLossCalculator.View;
using System;
using System.Windows.Input;

namespace PipelinePressureLossCalculator.ViewModel
{
    /// <summary>
    ///Логика обработки событий главной формы
    /// </summary>
    class MainViewModel:BaseViewModel {
        
        private static readonly ILog Log = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        public const string AppTitle = "Калькулятор потерь давления в трубопроводе";

        /// <summary>
        /// Название формы
        /// </summary>
        public string WindowTitle { get; set; }

        private string _pipelinePressureLossText;
        /// <summary>
        /// Потери давления (цель)
        /// </summary>
        public string PipelinePressureLossText
        {
            get => _pipelinePressureLossText;
            set {
                _pipelinePressureLossText = value;
                OnPropertyChanged();
            }
        }
        /// <summary>
        /// Расход
        /// </summary>
        public string ConsumptionText { get; set; }
        /// <summary>
        /// Длина трубы
        /// </summary>
        public string LengthText { get; set; }
        /// <summary>
        /// Внутренний диаметр трубы
        /// </summary>
        public string DiameterText { get; set; }
        /// <summary>
        /// Кинематический коэф. вязкости жидкости
        /// </summary>
        public string KinematicViscosityText { get; set; }
        /// <summary>
        /// Плотность жидкости
        /// </summary>
        public string DensityText { get; set; }
        /// <summary>
        /// Эквивалентная шероховатость
        /// </summary>
        public string EquivRoughnessText { get; set; }

        /// <summary>
        /// Команда нажатия на кнопку расчёта
        /// </summary>
        public ICommand CalcBtnClickCommand { get; }

        //Команды отображения справочных данных
        public ICommand ShowKinemViscosityRefDataCmd { get; }
        public ICommand ShowDensityRefDataCmd { get; }
        public ICommand ShowEquivRoughnessRefDataCmd { get; }


        public MainViewModel()            
        {
            WindowTitle = AppTitle;
            CalcBtnClickCommand = new DelegateCommand(arg=>CalcBtnClick());
            ShowKinemViscosityRefDataCmd = new DelegateCommand(arg => ShowKinemViscosityRefData());
            ShowDensityRefDataCmd = new DelegateCommand(arg => ShowDensityRefData());
            ShowEquivRoughnessRefDataCmd=new DelegateCommand(arg => ShowEquivRoughnessRefData());
            _pipelinePressureLossText = "цель";
        }



        /// <summary>
        /// Нажата кнопка расчёта
        /// </summary>
        private void CalcBtnClick() {
            Log.Debug(GetInputDataAsText());

            InputData inputData = new InputData();
            IConverter siConverter = new SiConverter();
            Calculator calculator = new Calculator();

            //конвертируем расход в CИ
            double consumption = Convert.ToDouble(ConsumptionText);
            consumption = siConverter.ConvertUnit("l/min", consumption);
            inputData.Consumption = consumption;
 
            //конвертируем диаметр в CИ
            double diameter = Convert.ToDouble(DiameterText);
            diameter = siConverter.ConvertUnit("mm", diameter);
            inputData.Diameter = diameter;

            //длина
            inputData.LengthPipeline = Convert.ToDouble(LengthText);
            //кинем.коэф.вязкости
            inputData.KinematicViscosity = Convert.ToDouble(KinematicViscosityText);
            //эквив.шероховатость
            inputData.EquivRoughness = Convert.ToDouble(EquivRoughnessText);
            //плотность
            inputData.Density = Convert.ToDouble(DensityText);

            //Задаём исходные данные для калькулятора
            calculator.InputData = inputData;
            //результат - потери давления
            OutputData outputData= calculator.GetPipelinePressureLoss();
            PipelinePressureLossText = Convert.ToString(outputData.PipelinePressureLoss);
        }

        private string GetInputDataAsText()
        {
            System.Text.StringBuilder sbl = new System.Text.StringBuilder();
            sbl.Append("Input data to UI:").Append(Environment.NewLine)
                .Append("Diameter=").Append(DiameterText).Append(Environment.NewLine)
                .Append("LengthPipeline=").Append(LengthText).Append(Environment.NewLine)
                .Append("Consumption=").Append(ConsumptionText).Append(Environment.NewLine)
                .Append("KinematicViscosity=").Append(KinematicViscosityText).Append(Environment.NewLine)
                .Append("Density=").Append(DensityText).Append(Environment.NewLine)
                .Append("EquivRoughness=").Append(EquivRoughnessText).Append(Environment.NewLine);
            return sbl.ToString();
        }

        /// <summary>
        /// Открыть Cправочные данные по кинем. вязкости
        /// </summary>
        private void ShowKinemViscosityRefData()
        {
            ShowRefData(RefDataType.KinematicViscosity);                      
        }
        /// <summary>
        /// Открыть Cправочные данные по плотности
        /// </summary>
        private void ShowDensityRefData()
        {
            ShowRefData(RefDataType.Density);
        }
        /// <summary>
        /// Открыть Cправочные данные по экв.шероховатости
        /// </summary>
        private void ShowEquivRoughnessRefData()
        {
            ShowRefData(RefDataType.EquivRoughness);
        }

        private void ShowRefData(RefDataType type) {
            RefDataViewModel vm = new RefDataViewModel(type);
            new ViewService.WindowService().ShowWindow<RefDataView>(vm);
            vm.ShowData();
        }
    }
}