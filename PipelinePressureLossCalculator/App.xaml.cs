﻿using PipelinePressureLossCalculator.ViewModel;
using System.Windows;

namespace PipelinePressureLossCalculator
{
    /// <summary>
    /// Логика взаимодействия для App.xaml
    /// </summary>
    public partial class App
    {
        protected override void OnStartup(StartupEventArgs e)
        {
            base.OnStartup(e);
            new ViewService.WindowService()
                .ShowWindow<View.MainView>(
                new MainViewModel());
        }
    }
}
