﻿namespace PipelinePressureLossCalculator.Model
{
    /// <summary>
    /// Выходные данные калькулятора
    /// </summary>
    public class OutputData
    {
        /// <summary>
        /// Потери давления
        /// </summary>
        public double PipelinePressureLoss { get; set; }
    }
}
