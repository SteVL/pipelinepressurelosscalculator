﻿namespace PipelinePressureLossCalculator.Model.ReferenceData
{
    /// <summary>
    /// Справочные данные по Экв. шероховатости
    /// </summary>
    class EquivRoughnessRefData:RefData, IRefData
    {
        private const string ImageFileName = "sherohovatost.gif";

        public RefData Load()
        {
            ImagePath = RefDataDir + ImageFileName;
            return this;
        }
    }
}
