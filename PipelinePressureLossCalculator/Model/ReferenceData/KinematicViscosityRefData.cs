﻿namespace PipelinePressureLossCalculator.Model.ReferenceData
{
    /// <summary>
    /// Справочные данные по Кинематической вязкости
    /// </summary>
    public class KinematicViscosityRefData : RefData, IRefData
    {
        private const string ImageFileName = "vyazkost.gif";

        public RefData Load()
        {
            ImagePath = RefDataDir + ImageFileName;
            return this;
        }
    }
}
