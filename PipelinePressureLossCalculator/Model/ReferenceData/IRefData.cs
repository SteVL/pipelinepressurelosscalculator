﻿namespace PipelinePressureLossCalculator.Model.ReferenceData
{
    /// <summary>
    /// Возможности действий со справочными данными
    /// </summary>
    interface IRefData
    {
        RefData Load();
    }
}
