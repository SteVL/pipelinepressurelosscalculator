﻿namespace PipelinePressureLossCalculator.Model.ReferenceData
{
    /// <summary>
    /// Справочные данные для пользователя
    /// </summary>
    public abstract class RefData
    {
        /// <summary>
        /// Каталог изображений справочных данных
        /// </summary>
        protected static readonly string RefDataDir = Util.Global.ImageDir + "ref-data/";       
        
        /// <summary>
        /// Путь к изображению
        /// </summary>
        public string ImagePath { get; protected set; }

   
    }
}
