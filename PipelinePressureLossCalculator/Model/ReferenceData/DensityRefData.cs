﻿namespace PipelinePressureLossCalculator.Model.ReferenceData
{
    /// <summary>
    /// Справочные данные по Плотности
    /// </summary>
    class DensityRefData:RefData,IRefData
    {
        private const string ImageFileName = "plotnost.gif";      

        public RefData Load()
        {
            ImagePath = RefDataDir + ImageFileName;
            return this;
        }
    }
}
