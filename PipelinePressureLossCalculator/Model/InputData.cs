﻿using System;
using System.Text;

namespace PipelinePressureLossCalculator.Model
{   
    /// <summary>
    /// Входные данные для калькулятора
    /// </summary>
    public class InputData
    {
        /// <summary>
        /// Расход жидкости
        /// </summary>
        public double Consumption { get; set; }
        /// <summary>
        /// Длина трубы
        /// </summary>
        public double LengthPipeline { get; set; }
        /// <summary>
        /// Внутренний диаметр трубы
        /// </summary>
        public double Diameter { get; set; }
        /// <summary>
        /// Кинематический коэф. вязкости жидкости
        /// </summary>
        public double KinematicViscosity { get; set; }
        /// <summary>
        /// Плотность жидкости
        /// </summary>
        public double Density { get; set; }
        /// <summary>
        /// Эквивалентная шероховатость
        /// </summary>
        public double EquivRoughness { get; set; }


        public override string ToString()
        {
            StringBuilder sbl = new StringBuilder();
            sbl.Append("Input data to Calculator Model:").Append(Environment.NewLine)
                .Append("Diameter=").Append(Diameter).Append(Environment.NewLine)
                .Append("LengthPipeline=").Append(LengthPipeline).Append(Environment.NewLine)
                .Append("Consumption=").Append(Consumption).Append(Environment.NewLine)
                .Append("KinematicViscosity=").Append(KinematicViscosity).Append(Environment.NewLine)
                .Append("Density=").Append(Density).Append(Environment.NewLine)
                .Append("EquivRoughness=").Append(EquivRoughness).Append(Environment.NewLine);
            return sbl.ToString();
        }
    }


}
