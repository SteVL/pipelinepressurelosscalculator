﻿using log4net;
using System;

namespace PipelinePressureLossCalculator.Model
{
    /// <summary>
    /// Калькулятор для расчёта потерь давления в трубопроводе
    /// </summary>
    public class Calculator
    {
        private static readonly ILog Log = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
        //ускорение свободного падения
        private const double Gravity = 9.81;

        /// <summary>
        /// Входные данные в системе СИ
        /// </summary>
        public InputData InputData { get; set; }

        /// <summary>
        /// Вычисляет потери давления в трубе
        /// </summary>
        /// <returns></returns>
        public OutputData GetPipelinePressureLoss()
        {
            /*
             Алгоритм основан на данных в этом источнике
             http://www.hydro-pnevmo.ru/ppd_form.php
             */

            Log.Info(InputData);

            OutputData outputData = new OutputData();

            double area = (Math.PI * Math.Pow(InputData.Diameter, 2)) / 4;
            Log.Debug("Площадь живого сечения, кв.м ="+area);
            double velocity = InputData.Consumption / area;
            Log.Debug("Ср.скорость потока =" + velocity);
            double radiusG = InputData.Diameter / 4;
            Log.Debug("Гидравлический радиус =" + radiusG);
            double reynoldsNumber = (velocity * 4 * radiusG) / InputData.KinematicViscosity;
            Log.Debug("Число Рейнольдса =" + reynoldsNumber);

            //определяем коэффициент гидравлического трения
            double lambda;
            //увы, одна возможная лишняя операция.
            // коэф для ламинарного режима
            double lambda1 = 64 / reynoldsNumber;
            //коэф. для турбулентного течение
            double lambda2 = 0.11 * Math.Pow(
                    (InputData.EquivRoughness / InputData.Diameter) + (68 / reynoldsNumber), 0.25);        
            //Ламинарное течение
            if (reynoldsNumber < 2000)
            {
                Log.Debug("Ламинарное течение");
                lambda = lambda1;
            }
            //турбулентное течение R>4000
            else if (reynoldsNumber < 4000) {
                Log.Debug("Турбулентное течение");
                lambda = lambda2;
            }       
            // Переходный режим 2000 < Re < 4000
            else
            {
                Log.Debug("Переходный режим");
                //значение хи
                double xi=Math.Pow(Math.Sin(
                    Math.PI/2*(reynoldsNumber/2000-1)),2);
                Log.Debug("Хи-коэффициент ="+ xi);
                lambda = (1-xi)*lambda1+xi*lambda2;
            }            
            Log.Debug("Коэф.гидравл.трения = " + lambda);

            //потери напора по длине трубы
            double dh = lambda * (InputData.LengthPipeline / InputData.Diameter) * (Math.Pow(velocity,2)/(2*Gravity));
            Log.Debug("Потери напора по длине трубы = " + dh);
            
            //цель        
            double pipeLinePressureLoss = dh* InputData.Density * Gravity;
            Log.Debug("Потери давления =" + pipeLinePressureLoss);

            outputData.PipelinePressureLoss = pipeLinePressureLoss;
            return outputData;
        }

        /// <summary>
        /// Предназначен для теста расчёта потерь напора.
        /// Решение Issue #4 
        /// </summary>
        /// <returns></returns>
        public double GetDeltaH() {
            //значения взяты из логов
            double lambda= 306278.3117521;
            double V= 0.000890015982630947;
            double lengthPipeline= 123.45;
            double diameter= 0.54;
            //hi = λ×l/d×v2/2g
            double res = lambda * (lengthPipeline / diameter) * (Math.Pow(V, 2) / (2 * Gravity));
            return res;
        }
    }
}
