﻿namespace PipelinePressureLossCalculator.Model.Converter
{
    /// <summary>
    /// Конвертер единиц измерения
    /// </summary>
    interface IConverter
    {
        /// <summary>
        /// Конвертирует ед.изм
        /// </summary>
        /// <param name="sourceUnit">исходная ед. изм.</param>
        /// <param name="value">число для преобразования</param>
        /// <returns>преобразованное значение</returns>
        double ConvertUnit(string sourceUnit, double value);
    }
}
