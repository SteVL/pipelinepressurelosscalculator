﻿using System;

namespace PipelinePressureLossCalculator.Model.Converter
{
    /// <summary>
    /// Конвертер в СИ
    /// </summary>
   public class SiConverter : IConverter
    {
        /*
         Пока просто реализуем конвертацию в лоб.
        */
        private static readonly string[] SupportedUnits = { "mm", "l/min" };

        /// <summary>
        /// Конвертирует в СИ
        /// <para>Входные данные:</para>
        /// мм - mm;
        /// л/мин - l/min;
        /// </summary>
        /// <param name="sourceUnit">исходная ед. изм.</param>
        /// <param name="value">число для преобразования</param>
        /// <returns>значение СИ</returns>
        public double ConvertUnit(string sourceUnit, double value)
        {
            bool existed = IsExisted(sourceUnit);
            //если не найдено ед.изм
            if (!existed) {
                throw new Exception("The unit of measurement isn't supported.");
            }
            double res = 0;
            switch (sourceUnit) {
                case "mm":
                    res= value*Math.Pow(10, -3);
                    break;
                case "l/min":
                    res = value/60000;
                    break;             
            }
            return res;
        }

        /// <summary>
        /// Проверяет существование ед.изм в данной реализации
        /// </summary>
        /// <param name="unit"></param>
        /// <returns></returns>
        private bool IsExisted(string unit)
        {
            foreach (string supported in SupportedUnits)
            {
                if (unit.Equals(supported))
                    return true;
            }
            return false;
        }

    }
}
