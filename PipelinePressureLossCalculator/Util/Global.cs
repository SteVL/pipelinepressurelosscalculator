﻿using System;

namespace PipelinePressureLossCalculator.Util
{
    static class Global
    {
        private static readonly string ResourceDir = AppDomain.CurrentDomain.BaseDirectory + "resources/";
        public static readonly string ImageDir = ResourceDir + "img/";
    }
}
