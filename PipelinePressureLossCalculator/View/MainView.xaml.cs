﻿using log4net;


namespace PipelinePressureLossCalculator.View
{
    /// <summary>
    /// Логика взаимодействия для MainView.xaml
    /// </summary>
    public partial class MainView
    {
        private static readonly ILog Log = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
        
        public MainView()
        {
            InitializeComponent();
            log4net.Config.XmlConfigurator.Configure();
            Log.Info("        =============  Started Logging  =============        ");
            Log.Debug("MainView");
        }
    }
}
