﻿using PipelinePressureLossCalculator.ViewModel;
using System.Windows;

namespace PipelinePressureLossCalculator.ViewService
{
    /// <summary>
    /// Cлужба работы с окнами (View)
    /// </summary>
    class WindowService
    {
        public void ShowWindow<T>(BaseViewModel viewModel) where T : Window, new()
        {
            T view = new T {DataContext = viewModel};
            if(view.GetType() != typeof(View.MainView)){
                view.Owner = Application.Current.MainWindow;
            }
            view.Show();
        }
    }
}
