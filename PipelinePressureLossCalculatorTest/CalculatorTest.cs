﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using PipelinePressureLossCalculator.Model;
namespace PipelinePressureLossCalculatorTest
{
    [TestClass]
    public class CalculatorTest
    {
        [TestMethod]
        public void GetDeltaH()
        {
            double expected = 2.8268917061092;
            Calculator calculator = new Calculator();
            double delta= 0.00001;
            Assert.AreEqual(expected, calculator.GetDeltaH(),delta);
        }
    }
}
